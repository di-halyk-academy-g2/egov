export const SERVICES_PAGE_ROUTE = '/';
export const SIGNIN_PAGE_ROUTE = '/signin';
export const SIGNUP_PAGE_ROUTE = '/signup';
export const PROFILE_PAGE_ROUTE = '/profile';
export const PROFILE_EDIT_PAGE_ROUTE = '/profile/edit';
