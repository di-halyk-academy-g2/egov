import ServicesPage from './components/ServicesPage/ServicesPage';
import SignInPage from './components/SignInPage/SignInPage';
import SignUpPage from './components/SignUpPage/SignUpPage';
import ProfilePage from './components/ProfilePage/ProfilePage';
import ProfileEditPage from './components/ProfileEditPage/ProfileEditPage';

import {
    PROFILE_EDIT_PAGE_ROUTE,
    PROFILE_PAGE_ROUTE,
    SERVICES_PAGE_ROUTE,
    SIGNIN_PAGE_ROUTE,
    SIGNUP_PAGE_ROUTE
} from './utils/consts';


export const publicRoutes = [
    { path: SERVICES_PAGE_ROUTE, Component: ServicesPage },
    { path: SIGNIN_PAGE_ROUTE, Component: SignInPage },
    { path: SIGNUP_PAGE_ROUTE, Component: SignUpPage },
    { path: PROFILE_PAGE_ROUTE, Component: ProfilePage },
    { path: PROFILE_EDIT_PAGE_ROUTE, Component: ProfileEditPage }
]
