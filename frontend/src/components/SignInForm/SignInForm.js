import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './SignInForm.css';
import { SERVICES_PAGE_ROUTE } from '../../utils/consts';

const SignInForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8000/auth/login/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem('access_token', data.access);
        localStorage.setItem('refresh_token', data.refresh);
        setError('');
        navigate(SERVICES_PAGE_ROUTE);
      } else if (response.status === 401) {
        setError('Неверное имя пользователя или пароль');
      } else {
        setError('Ошибка входа в систему');
      }
    } catch (error) {
      console.error('Ошибка запроса:', error);
      setError('Ошибка входа в систему');
    }
  };

  return (
    <div className="SignInForm">
      <form onSubmit={handleSubmit}>
        <fieldset className="mb-3">
          <label htmlFor='username' className='form-label'>Имя пользователя</label>
          <input type='text' placeholder='Введите имя пользователя' 
            id='username' className='form-control'
            value={username} onChange={(e) => setUsername(e.target.value)} 
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='password' className='form-label'>Пароль</label>
          <input type='password' placeholder='Введите пароль' 
            id='password' className='form-control' 
            value={password} onChange={(e) => setPassword(e.target.value)}
          />
        </fieldset>
        <button className='btn btn-success'>Войти в систему</button>

        {error && <div className="error-message">{error}</div>}
      </form>
    </div>
  );
};

export default SignInForm;
