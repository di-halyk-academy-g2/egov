import React, { useState, useEffect } from 'react';
import './ServicesPage.css';
import Header from '../Header/Header';
import { PROFILE_EDIT_PAGE_ROUTE } from '../../utils/consts';

const ServicesPage = ({ isAuthenticated, user }) => {

  return (
    <div className="ServicesPage">
      <Header isAuthenticated={isAuthenticated} user={user} />
      <div className='service-main main'>
        <h1 className='mb-5'>Сервисы</h1>
        <div>
          {user && (
            user.is_staff ? (
              <div>
                <h3>Все заявки:</h3>
                <button>Посмотреть все заявки</button>
              </div>
            ) : (
              <div>
                <a href={PROFILE_EDIT_PAGE_ROUTE} className='btn btn-warning'>Оставить заявку на профильные данные</a>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
}

export default ServicesPage;
