import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { publicRoutes } from '../../routes';

const AppRouter = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [profileData, setProfileData] = useState(null);

  useEffect(() => {
    const checkAccessTokenValidity = async () => {
      const accessToken = localStorage.getItem('access_token');
      
      if (accessToken) {
        try {
          const response = await fetch('http://localhost:8000/auth/token/verify/', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              token: accessToken,
            }),
          });

          if (response.ok) {
            setIsAuthenticated(true);
          } else {
            setIsAuthenticated(false);
          }
        } catch (error) {
          console.error('Error checking token validity:', error);
          setIsAuthenticated(false);
        }
      } else {
        setIsAuthenticated(false);
      }
    };
    checkAccessTokenValidity();
  }, []);
  
  useEffect(() => {
    const fetchProfileData = async () => {
      const accessToken = localStorage.getItem('access_token');

      if (accessToken) {
        try {
          const response = await fetch('http://localhost:8000/auth/', {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${accessToken}`,
            },
          });

          if (response.ok) {
            const data = await response.json();
            setProfileData(data);
          } else {
            console.error('Error fetching profile data:', response.statusText);
          }
        } catch (error) {
          console.error('Error fetching profile data:', error);
        }
      }
    };

    fetchProfileData();
  }, []);

  return (
    <Routes>
      {publicRoutes.map(({ path, Component }) =>
        <Route 
          key={ path } 
          path={ path } 
          element={ <Component isAuthenticated={isAuthenticated} user={profileData} /> }
          exact 
        />
      )}
    </Routes>
  );
}

export default AppRouter;
