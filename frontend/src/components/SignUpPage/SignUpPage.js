import React from 'react';
import './SignUpPage.css';
import Header from '../Header/Header';
import SignUpForm from '../SignUpForm/SignUpForm';

const SignUpPage = ({ isAuthenticated, user }) => (
  <div className="SignUpPage">
    <Header isAuthenticated={isAuthenticated} user={user} />
    <section className='signin-main main'>
      <h1 className='mb-5'>Создать профиль на портале</h1>
      <SignUpForm />
    </section>
  </div>
);

export default SignUpPage;
