import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './ProfileEditPage.css';
import Header from '../Header/Header';
import { SERVICES_PAGE_ROUTE } from '../../utils/consts';

const ProfileEditPage = ({ isAuthenticated, user }) => {
  const [username, setUsername] = useState('');
  // const [password, setPassword] = useState(pass);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [about, setAbout] = useState('');
  const [educationList, setEducationList] = useState([]);
  const [selectedEducation, setSelectedEducation] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();
  const token = localStorage.getItem('access_token');

  useEffect(() => {
    if (user) {
      setUsername(user.username);
      setFirstName(user.first_name);
      setLastName(user.last_name);
      setEmail(user.email);
      setAbout(user.about);
      setSelectedEducation(user.education);
    }
  }, [user]);

  useEffect(() => {
    const fetchEducationList = async () => {
      try {
        const response = await fetch('http://localhost:8000/auth/educations/');
        const data = await response.json();
        setEducationList(data);
      } catch (error) {
        console.error('Error fetching education list:', error);
      }
    };

    fetchEducationList();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();


    console.log(JSON.stringify({
      username,
      first_name: firstName,
      last_name: lastName,
      email,
      about,
      education: selectedEducation,
      education_degree: null,
      user: user.id,
    }))

    try {
      const response = await fetch('http://localhost:8000/auth/application/send/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          username,
          first_name: firstName,
          last_name: lastName,
          email,
          about,
          education: selectedEducation,
        }),
      });

      if (response.ok) {
        alert('Заявка принята успешно!');
        navigate(SERVICES_PAGE_ROUTE); 
      } else {
        const data = await response.json();
        setError(data.error || 'Произошла ошибка при отправке заявки.');
      }
    } catch (error) {
      console.error('Error submitting application:', error);
      setError('Произошла ошибка при отправке заявки.');
    }
  };

  return (
    <div className="ProfileEditPage">
      <Header isAuthenticated={isAuthenticated} user={user} />
      <div className='profile-edit-main main'>
        <h1 className='mb-5'>Запрос на измениение профиля</h1>
        <form className='profile-edit-form' onSubmit={handleSubmit}>
          <fieldset className="mb-3">
            <label htmlFor='username' className='form-label'>Имя пользователя</label>
            <input type='text' placeholder='Введите имя пользователя'
                id='username' className='form-control'
                value={username} onChange={(e) => setUsername(e.target.value)}
            />
          </fieldset>
          {/* <fieldset className="mb-3">
            <label htmlFor='password' className='form-label'>Пароль</label>
            <input type='password' placeholder='Введите пароль'
              id='password' className='form-control'
              value={password} onChange={(e) => setPassword(e.target.value)}
            />
          </fieldset> */}
          <fieldset className="mb-3">
            <label htmlFor='firstName' className='form-label'>Имя</label>
            <input type='text' placeholder='Введите ваше имя'
              id='firstName' className='form-control'
              value={firstName} onChange={(e) => setFirstName(e.target.value)}
            />
          </fieldset>
          <fieldset className="mb-3">
            <label htmlFor='lastName' className='form-label'>Фамилия</label>
            <input type='text' placeholder='Введите вашу фамилию'
              id='lastName' className='form-control'
              value={lastName} onChange={(e) => setLastName(e.target.value)}
            />
          </fieldset>
          <fieldset className="mb-3">
            <label htmlFor='email' className='form-label'>Email</label>
            <input type='email' placeholder='Введите ваш email'
              id='email' className='form-control'
              value={email} onChange={(e) => setEmail(e.target.value)}
            />
          </fieldset>
          <fieldset className="mb-3">
            <label htmlFor='education' className='form-label'>Образование</label>
            <select id='education' className='form-control' value={selectedEducation} onChange={(e) => setSelectedEducation(e.target.value)} >
              <option value="">Выберите образование</option>
              {educationList && educationList.map((education) => (
                <option key={education.id} value={education.id}>
                  {education.name}
                </option>
              ))}
            </select>
          </fieldset>
          <fieldset className="mb-3">
            <label htmlFor='about' className='form-label'>О себе (Необязательно)</label>
            <textarea placeholder='Расскажите о себе'
              id='about' className='form-control'
              value={about} onChange={(e) => setAbout(e.target.value)}
            />
          </fieldset>
          <button className='btn btn-success mb-5' type='submit'>Проверить и отправить</button>

          {error && <div className="error-message mb-5">{error}</div>}
        </form>
      </div>
    </div>
  );
}

export default ProfileEditPage;
