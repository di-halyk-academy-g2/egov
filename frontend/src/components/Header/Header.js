import React from 'react';
import { useNavigate } from 'react-router-dom';
import './Header.css';
import { PROFILE_PAGE_ROUTE, SERVICES_PAGE_ROUTE, SIGNIN_PAGE_ROUTE, SIGNUP_PAGE_ROUTE } from '../../utils/consts';

const Header = ({ isAuthenticated, user }) => {
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');

    navigate(SERVICES_PAGE_ROUTE);
    window.location.reload();
  };

  return (
    <div className="header">
      <div>
        <a href={SERVICES_PAGE_ROUTE} className='header--item btn btn-light'>
          Services
        </a>
        <a href={PROFILE_PAGE_ROUTE} className='header--item btn btn-light'>
          Profile
        </a>
      </div>
      <div>
        {isAuthenticated ? (
          <button onClick={handleLogout} className='header--item btn btn-light'>
            Logout
          </button>
        ) : (
          <div>
            <a href={SIGNIN_PAGE_ROUTE} className='header--item btn btn-light'>
              Sign In
            </a>
            <a href={SIGNUP_PAGE_ROUTE} className='header--item btn btn-light'>
              Sign Up
            </a>
          </div>
        )}
      </div>
    </div>
  );
}

export default Header;
