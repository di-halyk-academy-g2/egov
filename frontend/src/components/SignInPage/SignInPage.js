import React from 'react';
import './SignInPage.css';
import Header from '../Header/Header';
import SignInForm from '../SignInForm/SignInForm';

const SignInPage = ({ isAuthenticated, user }) => (
  <div className="SignInPage">
    <Header isAuthenticated={isAuthenticated} user={user} />
    <section className='signin-main main'>
      <h1 className='mb-5'>Вход на портал</h1>
      <SignInForm />
    </section>
  </div>
);

export default SignInPage;
