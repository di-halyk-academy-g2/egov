import React, { useState, useEffect } from 'react';
import './ProfilePage.css';
import Header from '../Header/Header';

const ProfilePage = ({ isAuthenticated, user }) => { 

  return (
    <div className="ProfilePage">
      <Header isAuthenticated={isAuthenticated} user={user} />
      <div className='profile-main main'>
        {user && (
          <div>
            <h1 className='mb-5'>Профиль</h1>
            <h3 className='profile--item'><span>Имя пользователя:</span> {user.username}</h3>
            <h3 className='profile--item'><span>Полное имя:</span> {user.first_name} {user.last_name}</h3>
            <h3 className='profile--item'><span>Эл. почта:</span> {user.email}</h3>
            {user.education && (
              <h3 className='profile--item'><span>Образование:</span> {user.education.name}</h3>
            )}
            

            {/* Другие данные профиля */}
          </div>
        )}
      </div>
    </div>
  );
}

export default ProfilePage;
