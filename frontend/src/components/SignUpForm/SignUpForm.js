import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './SignUpForm.css';
import { SIGNIN_PAGE_ROUTE } from '../../utils/consts';

const SignUpForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [about, setAbout] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8000/auth/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username,
          password,
          first_name: firstName,
          last_name: lastName,
          email,
          about,
        }),
      });

      if (!response.ok) {
        throw new Error('Ошибка при отправке данных');
      }

      const data = await response.json();
      console.log('Response:', data);

      navigate(SIGNIN_PAGE_ROUTE);
    } catch (error) {
      console.error('Error:', error.message);
      setError('Ошибка при отправке данных');
    }
  };

  return (
    <div className="SignUpForm">
      <form onSubmit={handleSubmit}>
        <fieldset className="mb-3">
          <label htmlFor='username' className='form-label'>Имя пользователя</label>
          <input type='text' placeholder='Введите имя пользователя'
            id='username' className='form-control'
            value={username} onChange={(e) => setUsername(e.target.value)}
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='password' className='form-label'>Пароль</label>
          <input type='password' placeholder='Введите пароль'
            id='password' className='form-control'
            value={password} onChange={(e) => setPassword(e.target.value)}
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='firstName' className='form-label'>Имя</label>
          <input type='text' placeholder='Введите ваше имя'
            id='firstName' className='form-control'
            value={firstName} onChange={(e) => setFirstName(e.target.value)}
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='lastName' className='form-label'>Фамилия</label>
          <input type='text' placeholder='Введите вашу фамилию'
            id='lastName' className='form-control'
            value={lastName} onChange={(e) => setLastName(e.target.value)}
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='email' className='form-label'>Email</label>
          <input type='email' placeholder='Введите ваш email'
            id='email' className='form-control'
            value={email} onChange={(e) => setEmail(e.target.value)}
          />
        </fieldset>
        <fieldset className="mb-3">
          <label htmlFor='about' className='form-label'>О себе (Необязательно)</label>
          <textarea placeholder='Расскажите о себе'
            id='about' className='form-control'
            value={about} onChange={(e) => setAbout(e.target.value)}
          />
        </fieldset>
        <button className='btn btn-success'>Создать профиль</button>

        {error && <div className="error-message">{error}</div>}
      </form>
    </div>
  );
}

export default SignUpForm;
