from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework import status
from rest_framework.views import APIView

from .models import User, UserTemplate
from .serializers import *


class UserViewSet(GenericViewSet, mixins.CreateModelMixin, ):
    def get_queryset(self):
        if self.action == 'retrieve' or self.action == 'update':
            return User.objects.get(id=self.request.user.id)
        return User.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreateSerializer
        elif self.action in ['retrieve', 'update']:
            return UserSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_queryset()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = self.get_queryset().first()
        serializer = self.get_serializer(instance, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class UserTemplateViewSet(GenericViewSet, mixins.CreateModelMixin, ):
    def get_queryset(self):
        return UserTemplate.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            return UserTemplateCreateSerializer
        elif self.action == 'retrieve':
            return UserTemplateSerializer
        
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_queryset()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class UserTemplateTransferAPIView(APIView):
    permission_classes = (IsAdminUser, IsAuthenticated)

    def get(self, request, user_template_id, *args, **kwargs):
        user_template = UserTemplate.objects.get(id=user_template_id)
        serializer = UserTemplateSerializer(user_template)

        return Response(serializer.data)
    
    def post(self, request, user_template_id, *args, **kwargs):
        user_template = UserTemplate.objects.get(id=user_template_id)
        user = user_template.user
        user.email = user_template.email    
        user.about = user_template.about
        user.education = user_template.education
        user.education_degree = user_template.education_degree
        user.username = user_template.username
        user.first_name = user_template.first_name
        user.last_name = user_template.last_name
        user.save()

        serializer = UserSerializer(user)
        return Response({'data': serializer.data}) 

class EducationAPIView(APIView):
    def get(self, request):
        educations = Education.objects.all()
        serializer = EducationSerializer(educations, many=True)

        return Response(serializer.data)
    
    def post(self, request):
        try:
            name = request.data['name']
        except KeyError:
            return Response({'error': 'Missing name field in request data'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            education = Education.objects.create(name=name)
            serializer = EducationSerializer(education)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

