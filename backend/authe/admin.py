from django.contrib import admin
from .models import *

@admin.register(Education)
class EducationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name', )

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name', 'email', 'is_staff', )
    search_fields = ('username', 'first_name', 'last_name', 'email', )
    list_filter = ('education', )

admin.site.register(UserTemplate)