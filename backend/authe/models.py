from django.contrib.auth.models import AbstractUser
from django.db import models


class Education(models.Model):
    name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.name


class User(AbstractUser):
    EDUCATION_DEGREE_CHOICES = (
        ('bachelor', 'бакалавр'),
        ('master', 'магистр'),
    )

    about = models.TextField(null=True, blank=True)
    education = models.ForeignKey(Education, on_delete=models.SET_NULL, null=True, blank=True)
    education_degree = models.CharField(choices=EDUCATION_DEGREE_CHOICES, max_length=30, null=True, blank=True)

    groups = models.ManyToManyField(
        'auth.Group',
        related_name='users',
        blank=True,
        verbose_name='groups',
        help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.',
    )
    user_permissions = models.ManyToManyField(
        'auth.Permission',
        related_name='users',
        blank=True,
        verbose_name='user permissions',
        help_text='Specific permissions for this user.',
        error_messages={
            'add': 'The permission you are trying to add already exists for the user.',
            'remove': 'The permission you are trying to remove does not exist for the user.',
        },
    )


class UserTemplate(models.Model):
    EDUCATION_DEGREE_CHOICES = (
        ('bachelor', 'бакалавр'),
        ('master', 'магистр'),
    )

    about = models.TextField(null=True, blank=True)
    education = models.ForeignKey(Education, on_delete=models.SET_NULL, null=True, blank=True)
    education_degree = models.CharField(choices=EDUCATION_DEGREE_CHOICES, max_length=30, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    username = models.CharField(max_length=150, unique=True)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    email = models.EmailField(blank=True)