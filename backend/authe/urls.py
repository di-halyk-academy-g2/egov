from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView, TokenVerifyView)

from . import views

app_name = 'authe'

urlpatterns = [
    path('', views.UserViewSet.as_view({'get': 'retrieve', 'post': 'create', 'put': 'update',}), name='user'),
    path('login/', TokenObtainPairView.as_view(), name='user_login'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('application/send/', views.UserTemplateViewSet.as_view({'post': 'create', 'get': 'retrieve'}), name='send_application'),
    path('application/send/<int:user_template_id>/', views.UserTemplateTransferAPIView.as_view(), name='transfer'),
    path('educations/', views.EducationAPIView.as_view(), name='educations'),
]
