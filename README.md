# eGov Project

## Short Description
The eGov project is a comprehensive web application aimed at improving public services for Kazakhstani citizens. The project is a single solution combining frontend and backend based on Django and React. This provides a reliable and scalable infrastructure for effective data management and user interaction in government services.

On the eGov E-Government portal Kazakhstanis can apply for changes, corrections and additions to the registration records of birth, marriage and divorce.

## Project Structure
#### Backend:
1. **Root:**
   - `.gitignore`: Specifies ignored files.
   - `docker-compose.yml`: Configuration for Docker.
   - `Dockerfile`: Instructions for building Docker images.
   - `manage.py`: Django management script.
   - `README.md`: Project documentation.
   - `requirements.txt`: Python dependencies.

2. **authe:**
   - `admin.py`: Django admin configurations.
   - `apps.py`: Django app configuration.
   - `models.py`: Database models.
   - `serializers.py`: Serialization logic.
   - `tests.py`: Unit tests.
   - `urls.py`: URL patterns.
   - `views.py`: Request handling logic.
   - `migrations/`: Database migration files.
   - `__pycache__/`: Python bytecode cache.

3. **backend:**
   - `asgi.py`: Asynchronous Server Gateway Interface.
   - `settings.py`: Django project settings.
   - `urls.py`: URL routing.
   - `wsgi.py`: Web Server Gateway Interface.
   - `__pycache__/`: Python bytecode cache.
   
#### Frontend:
1. **Root:**
   - `node_modules/`: Node.js dependencies.
   - `public/`: Static files.
   - `src/`: Source code.
   - `.gitignore`: Specifies ignored files.
   - `package-lock.json`: Lock file for Node.js dependencies.
   - `package.json`: Node.js project configuration.
   - `README.md`: Frontend documentation.
   - `webpack.config.js`: Webpack configuration.

2. **src/ (Source Code):**
   - `components/`: React components and pages.
   - `utils/`: Helper utilities.

3. **Additional Commands:**
   - `npm start`: Run the project locally.
   - `npm build`: Create an optimized production build.
   - `npm test`: Run tests.

## Features

#### User Management

- **User Registration and Update:** Users can register using the `POST /` endpoint, and authenticated users can update their profiles using the `PUT /` endpoint.

- **User Authentication:** Token-based authentication is implemented for user login. Use the `POST /login/` endpoint to obtain a JWT token.

#### Application Submission

- **Application Submission:** Users can submit applications using the `POST /application/send/` endpoint, providing details such as email, about, education, etc.

- **Application Retrieval:** Users and administrators can retrieve submitted applications using the `GET /application/send/` endpoint.

- **Application Transfer:** Administrators can transfer user template data to an actual user using the `POST /application/send/<int:user_template_id>/` endpoint.

#### Education Information

- **Retrieve Education Data:** Users can retrieve a list of available education data using the `GET /educations/` endpoint.

- **Add Education Data:** Administrators can add new education data using the `POST /educations/` endpoint by providing the `name` field.

#### Security and Permissions

- **Token Refresh and Verify:** Users can refresh their authentication token using the `POST /token/refresh/` endpoint. Token verification is available through the `POST /token/verify/` endpoint.

- **Permissions:** Different endpoints have varying permission requirements. For example, creating a new user or submitting an application may require authentication, while transferring user template data may require admin privileges.

#### Error Handling

- **Detailed Error Responses:** The API provides detailed error responses, including status codes and specific error messages, to guide users in case of issues.

## Responsible People

### Backend
- **ID:** 20B030768, 20B030191, 20В030333, 20B030128
- **Full Name:** Saudabekov Alan, Baizhanov Erkin, Zholdaskaliyev Nurtay, Khasen Ernar

### Frontend
- **ID:** 20B030768
- **Full Name:** Saudabekov Alan

### Documentation
- **ID:** 20B030191
- **Full Name:** Baizhanov Erkin

## INSTALL

### Prerequisites
1. **Programming Language and Version:**
   - Python 3.x
   - Node.js 12.x

2. **External Libraries or Frameworks:**
   - Django 3.x
   - React 17.x

3. **Database Requirements:**
   - PostgreSQL 13.x

4. **Other Dependencies:**
   - Docker (optional, for containerized environments)

5. **Tools or Software:**
   - npm (Node Package Manager)
   - Docker Compose (for running multi-container Docker applications)


### Backend Installation

1. **Clone the Repository**
```bash
git clone https://gitlab.com/di-halyk-academy-g2/egov.git
```
2. **Navigate to the Backend Directory**
```bash
cd backend
```

3. **Run the Backend and Database using Docker Compose**
```bash
docker-compose up --build
```

### Frontend Installation

1. **Clone the Repository (no need if already cloned)**
```bash
git clone https://gitlab.com/di-halyk-academy-g2/egov.git
```

2. **Navigate to the Frontend Directory**
```bash
cd frontend
```

3. **Install Dependencies**
```bash
npm install
```

4. **Run the Frontend Locally**
```bash
npm start
```
After completing these steps, the frontend should be running locally, and you can interact with it at **http://localhost:3000** in your web browser.

## CONTRIBUTING

### Reporting Bugs/Issues

To report bugs or issues on our social network platform, follow these steps:

- **Provide Details:**
   Include steps to reproduce, expected vs. actual behavior, and any relevant screenshots or error messages.

- **Contact Us:**
   - Email: [baizhanoverkin@gmail.com](mailto:baizhanoverkin@gmail.com)
   - Social Media: Direct message on [Telegram](https://t.me/fuckufuckinfuck6)

- **Subject Line:**
   Use a clear subject line, e.g., "Bug Report: [Brief Description]."

- **Response Time:**
   We'll acknowledge promptly and investigate. Your feedback helps us improve.


### Contribution Guide

1. **Fork, Branch, Commit, Push, PR:**
   - Fork the repo.
   - Create a branch (`feature/new` or `bug/fix`).
   - Commit changes (`git commit -m "Your message"`).
   - Push to the branch (`git push origin feature/new`).
   - Submit a pull request to `main`.

2. **Standards:**
   - Follow coding standards.
   - Keep code clean and documented.

3. **Pull Requests:**
   - Link to an issue.
   - Brief description, ensure code passes tests.

4. **Code Reviews:**
   All contributions undergo review.